#!/usr/bin/python
# -*- coding: utf-8 -*-

import random as rd


class Trash():
    #Przetapialność, tłuczenie, przezroczystość, wilgotność, niebezpieczne, metaliczne, gęstość
    #te funkcje __gen tworzą obiekty - są one po to by generowana losowo masa śmieci miała sens - papier o wadze 50000kg jest bez sensu
    def __gen_Papier(self):
        paperType = rd.randint(1,2)
        if paperType == 1:
            return [0,0,0,1,0,0, rd.randint(90, 120)/100.0]
        return [0,0,0,1,0,0, rd.randint(30, 50)/100.0]

    def __gen_Plastik(self):
        return [1,0,rd.randint(0,1),0,0,0,rd.randint(80,230)/100.0]
        
    def __gen_Szklo(self):
        return [1,1,1,0,0,0,rd.randint(250,590)/100.0]
        
    def __gen_Zlom(self):
        return [1,0,0,0,0,1,rd.randint(750,1000)/100.0]

    def __gen_Baterie(self):
        batteryType = rd.randint(1,3)
        if batteryType == 1:
            return [0,0,0,0,1,1,rd.randint(210,250)/100.0]
        elif batteryType == 2:
            return [0, 0, 0, 0, 1, 1, rd.randint(510, 550) / 100.0]
        return [0,0,0,0,1,1,rd.randint(710,750)/100.0]

    def __init__(self):
        self.trash = list() #śmieć jest opisany listą, znamy jego: przetapialność, tłuczenie, przezroczystość, wilgotność, gęstość, masę - jego typ określi później drzewo decyzyjne
        trashType = rd.randint(1,100) #losowo generujemy jakiś typ śmiecia i dajemy mu jego właściwości, jest to zrobione z tego samego względu co zabezpieczenie przed papierem który waży 500000000kg
        if 46 <= trashType <= 65:
            self.trash = self.__gen_Papier()
            self.trash.append(float(rd.randint(5,50)))
        elif 1 <= trashType <= 45:
            self.trash = self.__gen_Plastik()
            self.trash.append(float(rd.randint(30,500)))
        elif 66 <= trashType <= 80:
            self.trash = self.__gen_Szklo()
            self.trash.append(float(rd.randint(300, 15000)))
        elif 81 <= trashType <= 88:
            self.trash = self.__gen_Baterie()
            self.trash.append(float(rd.randint(50,500)))
        else:
            self.trash = self.__gen_Zlom()
            self.trash.append(rd.randint(1, 5000000)/100.0)

    def __str__(self): #obiekt typu Trash zwraca stringa z opisem śmiecia
        return str(self.get_trash())

    def get_trash(self): #zwraca listę opisującą śmiecia
        return self.trash

    #objetosc = masa/gestosc || Dzielimy przez 1000 zeby miec litry(dm3)
    def get_volume(self): #zwraca objętość w litrach
        return float(self.trash[-1]/self.trash[-2]/1000)


if __name__ == "__main__":
    for i in range(100):
        trash = Trash()
        #print(trash)
        print( trash.get_trash() )
        print( trash.get_volume() )
