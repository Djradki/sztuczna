#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame as pg
import functions


class Road(pg.sprite.Sprite):

    def __init__(self, pos_x, pos_y):
        pg.sprite.Sprite.__init__(self)

        self.image = pg.image.load(functions.getPath() + '/images/road.png')

        self.rect = self.image.get_rect()
        self.rect.x = pos_x
        self.rect.y = pos_y
