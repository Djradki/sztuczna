#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame as pg
import functions
import trash
import copy


class House(pg.sprite.Sprite):

    def __init__(self, pos_x, pos_y):
        pg.sprite.Sprite.__init__(self)

        self.image = pg.image.load(functions.getPath() + '/images/house.png')

        self.rect = self.image.get_rect()
        self.rect.x = pos_x
        self.rect.y = pos_y
        self.__trash_capacity = 250  # litry
        self.__trash = list()  # lista śmieci, które są w pojemniku

    '''def check_is_full(self):  # tego nie używamy
        if self.__glass_trash + self.__paper_trash + self.__plastic_trash >= self.__trash_capacity:
            # print(f'przepelnienie - domek: ({self.rect.x}, {self.rect.y})')
            return True
        return False'''

    def generate_trash(self):  # generowanie śmieci
        currentTrashVol = 0.0  # aktualna objętość pojemnika na śmieci
        while 3:
            myTrash = trash.Trash()  # tworzenie śmiecia
            if self.__trash_capacity >= (currentTrashVol + myTrash.get_volume()):  # jeśli pojemność śmietnika jest
                                                        # mniejsza niż pojemność kosza + pojemność śmiecia to dodaj
                currentTrashVol += myTrash.get_volume()
                self.__trash.append(myTrash)
            else:  # jeśli śmieć nie mieści się do kosza to nie wrzucamy go i kończymy generowanie śmieci
                break

    def getTrash(self):  # zwraca listę śmieci
        return self.__trash

    def printTrash(self):  # wypisuje listę śmieci
        for item in self.__trash:
            print(item)

    def get_amount_of_trash(self):  # zwraca ilość śmieci (obiektów typu Trash)
        return len(self.__trash)
        
    def pickTrash(self):  # bierze śmieci - w sumie to zwraca listę śmieci z kosza i czyści ją - czyli czyści kosz
        smieci = copy.deepcopy(self.__trash)
        self.__trash.clear()
        return smieci


'''
if __name__ == "__main__":
    hos = House(10, 10)
    hos.generate_trash()
    hos.printTrash()
    listaTrashy = hos.getTrash()
    print("\n\n")
    print(hos.get_amount_of_trash())
'''