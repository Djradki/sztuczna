import pygame as pg
import sys
import functions as fun
import robot
import truck as tr
import a_star as ast
import time


class SceneBase:
    def __init__(self):
        self.next = self
    
    def ProcessInput(self, events, pressed_keys):
        print("uh-oh, you didn't override this in the child class")

    def Update(self):
        print("uh-oh, you didn't override this in the child class")

    def Render(self, screen):
        print("uh-oh, you didn't override this in the child class")

    def SwitchToScene(self, next_scene):
        self.next = next_scene
    
    def Terminate(self):
        self.SwitchToScene(None)


def run_game(width, height, fps, starting_scene):
    pg.init()
    screen = pg.display.set_mode((width, height))
    clock = pg.time.Clock()

    active_scene = starting_scene

    while active_scene is not None:
        pressed_keys = pg.key.get_pressed()
        
        # Event filtering 
        filtered_events = []
        for event in pg.event.get():
            quit_attempt = False
            if event.type == pg.QUIT:
                quit_attempt = True
            elif event.type == pg.KEYDOWN:
                alt_pressed = pressed_keys[pg.K_LALT] or \
                              pressed_keys[pg.K_RALT]
                if event.key == pg.K_ESCAPE:
                    quit_attempt = True
                elif event.key == pg.K_F4 and alt_pressed:
                    quit_attempt = True
            
            if quit_attempt:
                active_scene.Terminate()
            else:
                filtered_events.append(event)
        
        active_scene.ProcessInput(filtered_events, pressed_keys)
        active_scene.Update()
        active_scene.Render(screen)
        
        active_scene = active_scene.next
        
        pg.display.flip()
        clock.tick(fps)


class CityScene(SceneBase):
    def __init__(self):
        SceneBase.__init__(self)
        self.roads, self.houses, self.walls, self.dump, self.city_map,  self.size = fun.read_mapFile(fun.getPath()
                                                                                                     + '/maps/city3.txt')
        self.BACKGROUND = (0, 128, 0)
        self.WINDOW_WIDTH = self.size * 100
        self.WINDOW_HEIGHT = self.size * 100
        self.truck = tr.Truck(self.dump.rect.x, self.dump.rect.y, self.WINDOW_WIDTH, self.WINDOW_HEIGHT, self.walls, self.houses)
        self.graph = ast.AStarGraph(fun.get_walls(self.walls, self.houses, self.dump), 0,
                                    int(self.WINDOW_WIDTH/100) - 1)
        self.all_sprites_list = pg.sprite.Group()      

        self.all_sprites_list.add(self.dump)
        self.all_sprites_list.update()

        for road in self.roads:
            self.all_sprites_list.add(road)
        for house in self.houses:
            house.generate_trash()
            self.all_sprites_list.add(house)
        for wall in self.walls:
            self.all_sprites_list.add(wall)

        self.currentMove = list()
        self.path = list()
        self.MAX_ROUNDS = 3
        self.currentRound = 0


    def set_paths(self):
        self.currentMove.clear()
        self.path = fun.find_paths(self.graph, self.truck, self.houses, self.dump)

    def ProcessInput(self, events, pressed_keys):
        for event in events:
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()

    def Update(self):
        if self.currentRound >= self.MAX_ROUNDS:
            time.sleep(3)
            pg.quit()
            sys.exit()

        if not self.path and not self.currentMove:
            self.set_paths()

        if self.path and len(self.currentMove) == 0:
            self.currentMove = self.path.pop(0)

        if self.currentMove:
            move = self.currentMove.pop(0)

            if move is 'right':
                self.truck.move_right()
            elif move is 'left':
                self.truck.move_left()
            elif move is 'down':
                self.truck.move_down()
            elif move is 'up':
                self.truck.move_up()
            elif move is 'throw' and fun.is_near_dump(self.truck, self.dump):
                self.currentRound += 1
                for house in self.houses:
                    house.generate_trash()
                self.truck.throw_trash(self.dump)
                #run_game(400, 700, 60, DumpScene())
            elif move is 'take':
                house = fun.check_house_near(self.truck, self.houses)
                #percentage = self.truck.take_trash(house)
                self.truck.take_trash(house)
                time.sleep(0.75)

    def Render(self, screen):
        screen = pg.display.set_mode((self.WINDOW_WIDTH, self.WINDOW_HEIGHT), 0, 32)
        pg.display.set_caption('Śmieciarka')
        screen.fill(self.BACKGROUND)
        self.all_sprites_list.add(self.truck)
        self.all_sprites_list.update()
        self.all_sprites_list.draw(screen)
        
        time.sleep(0.125)


class DumpScene(SceneBase):
    def __init__(self):
        SceneBase.__init__(self)
        #  pg.display.set_caption('Wysypisko')
        self.BACKGROUND = (100, 100, 100)  # RGB
        self._nof_tiles_height = 7
        self._nof_tiles_width = 4
        self.WINDOW_WIDTH = self._nof_tiles_width * 100
        self.WINDOW_HEIGHT = self._nof_tiles_height * 100
        self._bottom_containers_y_pos = self.WINDOW_HEIGHT - 150
        self._robot_x_pos = self.WINDOW_WIDTH / 2 - 50
        self._robot_y_pos = 200
        self.truck = pg.image.load(fun.getPath()+'/images/truck.png')
        #  robot = pg.image.load(func.getPath()+'\\images\\wysypisko_imgs\\robot.png')
        self.my_robot = robot.Robot(self._robot_x_pos, self._robot_y_pos)
        self.container_papier = pg.image.load(fun.getPath()+'/images/wysypisko_imgs/papier_container2.png')
        self.container_plastik = pg.image.load(fun.getPath()+'/images/wysypisko_imgs/plastik_container2.png')
        self.container_szklo = pg.image.load(fun.getPath()+'/images/wysypisko_imgs/szklo_container2.png')
        self.container_zlom = pg.image.load(fun.getPath()+'/images/wysypisko_imgs/zlom_container2.png')
        self.question_mark = pg.image.load(fun.getPath()+'/images/wysypisko_imgs/question_mark.png')
        self.all_sprites_list = pg.sprite.Group()
        self.all_sprites_list.add(self.my_robot)
        
    def ProcessInput(self, events, pressed_keys):
        for event in events:
            if event.type == pg.KEYDOWN and event.key == pg.K_RETURN:
                # Move to the next scene when the user pressed Enter
                self.SwitchToScene(CityScene())
        
    def Update(self):
        pass
    
    def Render(self, screen):
        # rozmiary obrazka 267*365
        screen.fill(self.BACKGROUND)
        screen.blit(self.truck, [self.WINDOW_WIDTH / 2 - 50, -30])
        screen.blit(self.container_papier, [0, self._bottom_containers_y_pos])
        screen.blit(self.container_plastik, [100, self._bottom_containers_y_pos])
        screen.blit(self.container_szklo, [200, self._bottom_containers_y_pos])
        screen.blit(self.container_zlom, [300, self._bottom_containers_y_pos])        
        self.all_sprites_list.draw(screen)


if __name__ == '__main__':
    run_game(1920, 1080, 60, CityScene())
    #run_game(400, 700, 60, DumpScene())
