[Papier *x] [Wilgotne *d] [Gęstość *g] [0.9 *h]
 (attr ?d ?g ?x) (value ?g ?h)

[Papier *x] [Wilgotne *d] [Gęstość *g] [1.0 *h]
 (attr ?d ?g ?x) (value ?g ?h)

[Papier *x] [Wilgotne *d] [Gęstość *g] [1.1 *h]
 (attr ?d ?g ?x) (value ?g ?h)

[Papier *x] [Wilgotne *d] [Gęstość *g] [0.3 *h]
 (attr ?d ?g ?x) (value ?g ?h)

[Papier *x] [Wilgotne *d] [Gęstość *g] [0.4 *h]
 (attr ?d ?g ?x) (value ?g ?h)

[Papier *x] [Wilgotne *d] [Gęstość *g] [0.5 *h]
 (attr ?d ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [0.8 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [0.9 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [1.0 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [1.1 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [1.2 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [1.3 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [1.4 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [1.5 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [1.6 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [1.7 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [1.8 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [1.9 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [2.0 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [2.1 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [2.2 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Gęstość *g] [2.3 *h]
 (attr ?a ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [0.8 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [0.9 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [1.0 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [1.1 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [1.2 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [1.3 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [1.4 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [1.5 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [1.6 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [1.7 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [1.8 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [1.9 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [2.0 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [2.1 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [2.2 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Plastik *x] [Przetapialne *a] [Przezroczyste *c] [Gęstość *g] [2.3 *h]
 (attr ?a ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [2.5 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [2.6 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [2.7 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [2.8 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [2.9 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [3.0 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [3.1 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [3.2 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [3.3 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [3.4 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [3.5 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [3.6 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [3.7 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [3.8 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [3.9 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [4.0 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [4.1 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [4.2 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [4.3 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [4.4 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [4.5 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [4.6 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [4.7 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [4.8 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [4.9 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [5.0 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [5.1 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [5.2 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [5.3 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [5.4 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [5.5 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [5.6 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [5.7 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [5.8 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Szkło *x] [Przetapialne *a] [Tłuczenie *b] [Przezroczyste *c] [Gęstość *g] [5.9 *h]
 (attr ?a ?b ?c ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [7.5 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [7.6 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [7.7 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [7.8 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [7.9 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [8.0 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [8.1 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [8.2 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [8.3 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [8.4 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [8.5 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [8.6 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [8.7 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [8.8 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [8.9 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [9.0 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Złom *x] [Przetapialne *a] [Metaliczny *f] [Gęstość *g] [9.1 *h]
 (attr ?a ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [2.1 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [2.2 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [2.3 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [2.4 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [2.5 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [5.1 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [5.2 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [5.3 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [5.4 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [5.5 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [7.1 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [7.2 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [7.3 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [7.4 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

[Bateria *x] [Niebezpieczne *e] [Metaliczny *f] [Gęstość *g] [7.5 *h]
 (attr ?e ?f ?g ?x) (value ?g ?h)

