import os
import re
path = os.path.dirname(os.path.abspath(__file__))
#przetapialność, tłuczenie, przezroczystość, wilgotność, gęstość
#
with open(path+"/dataCGIF.txt", "r", encoding="utf-8") as f:
    content = f.readlines()
data = [x.strip() for x in content]
while("" in data) : 
    data.remove("")
f.close
for i in range(len(data)):
    tmp = list()
    if "Przetapialne" in data[i]:
        tmp.append(1)   
    else:
        tmp.append(0)   
    if "Tłuczenie" in data[i]:
        tmp.append(1)   
    else:
        tmp.append(0)  
    if "Przezroczyste" in data[i]:
        tmp.append(1)   
    else:
        tmp.append(0)  
    if "Wilgotne" in data[i]:
        tmp.append(1)   
    else:
        tmp.append(0)   
    tmp.append(float(''.join(map(str,re.findall(r"[-+]?\d*\.\d+|\d+", data[i])))))

    if "Papier" in data[i]:
        tmp.append("Papier")
    elif "Złom" in data[i]:
        tmp.append("Złom")
    elif "Plastik" in data[i]:
        tmp.append("Plastik")
    elif "Szkło" in data[i]:
        tmp.append("Szkło")
    data[i] = tmp

f = open(path+"/TD.py", "w+", encoding="utf-8")
f.write("trainingSet = "+str(data))
f.close()






