from PIL import Image, ImageDraw, ImageFont
import os
import sys
import random
sys.path.insert(0, '../')

import trainingData

data = trainingData.trainingSet
datalen = len(data)

#przetapialność, tłuczenie, przezroczystość, wilgotność, gęstość

path = os.path.dirname(os.path.abspath(__file__))

for i in range(datalen):
    tmpList = list()
    tmpList = [0]*4
    itemname = data[i][5]
    filename = itemname.lower() + str(int(data[i][4]*10)) + "_" + str(i) + ".png"
    if data[i][0] != 0:
        tmpList[0] = 1
    if data[i][1] != 0:
        tmpList[1] = 1
    if data[i][2] != 0:
        tmpList[2] = 1
    if data[i][3] != 0:
        tmpList[3] = 1
    if tmpList.count(1) == 1:
        img = path+"/img_schemas/schema1.png"
    elif tmpList.count(1) == 2:
        img = path+"/img_schemas/schema2.png"
    elif tmpList.count(1) == 3:
        img = path+"/img_schemas/schema3.png"
    '''elif tmpList.count(1) == 4:
        img = path+"/img_schemas/schema4.png"'''
    image = Image.open(img)
    font_type = ImageFont.truetype("Arial.ttf", 18)
    draw = ImageDraw.Draw(image)

    if tmpList.count(1) == 1:
        tmpValList = list()
        if tmpList[0] == 1:
            tmpValList.append("Przetapialność")
        if tmpList[1] == 1:
            tmpValList.append("Tłuczenie")
        if tmpList[2] == 1:
            tmpValList.append("Przezroczystość")
        if tmpList[3] == 1:
            tmpValList.append("Wilgotność")
        draw.text(xy=(35,116),text=data[i][5],fill=(0,0,0),font=font_type)
        draw.text(xy=(177,116), text="Attr",fill=(0,0,0),font=font_type)
        draw.text(xy=(253,32),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(253,208),text="Gęstość",fill=(0,0,0),font=font_type)
        draw.text(xy=(420,208),text="Value",fill=(0,0,0),font=font_type)
        draw.text(xy=(551,208),text=str(data[i][4]),fill=(0,0,0),font=font_type)
        image.save(path+"/img_output/"+filename)

    if tmpList.count(1) == 2:
        tmpValList = list()
        if tmpList[0] == 1:
            tmpValList.append("Przetapialność")
        if tmpList[1] == 1:
            tmpValList.append("Tłuczenie")
        if tmpList[2] == 1:
            tmpValList.append("Przezroczystość")
        if tmpList[3] == 1:
            tmpValList.append("Wilgotność")
        draw.text(xy=(25,118),text=data[i][5],fill=(0,0,0),font=font_type)
        draw.text(xy=(183,118), text="Attr",fill=(0,0,0),font=font_type)
        draw.text(xy=(254,31),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(254,120),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(421,206),text="Value",fill=(0,0,0),font=font_type)
        draw.text(xy=(550,206),text=str(data[i][4]),fill=(0,0,0),font=font_type)
        draw.text(xy=(254,206),text="Gęstość",fill=(0,0,0),font=font_type)
        image.save(path+"/img_output/"+filename)

    if tmpList.count(1) == 3:
        tmpValList = list()
        if tmpList[0] == 1:
            tmpValList.append("Przetapialność")
        if tmpList[1] == 1:
            tmpValList.append("Tłuczenie")
        if tmpList[2] == 1:
            tmpValList.append("Przezroczystość")
        if tmpList[3] == 1:
            tmpValList.append("Wilgotność")
        draw.text(xy=(20,114),text=data[i][5],fill=(0,0,0),font=font_type)
        draw.text(xy=(170,114), text="Attr",fill=(0,0,0),font=font_type)
        draw.text(xy=(265,30),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(265,114),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(265,190),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(420,270),text="Value",fill=(0,0,0),font=font_type)
        draw.text(xy=(550,270),text=str(data[i][4]),fill=(0,0,0),font=font_type)
        draw.text(xy=(265,270),text="Gęstość",fill=(0,0,0),font=font_type)
        image.save(path+"/img_output/"+filename)
