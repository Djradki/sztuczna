import sys
sys.path.insert(0, '../')

import trainingData

data = trainingData.trainingSet

#za dużo plusów, żeby się nie gubić ++++++++++++++++++++

def trainingDataToLF():
    L = list()
    for i in range(len(data)):
        n = 1
        tmp = "[" + data[i][5] + "]¬(Attr)" + "-\n"
        if data[i][0] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Przetapialne" + "]\n"
            n = n + 1
        if data[i][1] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Tłuczenie" + "]\n"
            n = n + 1
        if data[i][2] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Przezroczyste" + "]\n"
            n = n + 1
        if data[i][3] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Wilgotne" + "]\n"
            n = n + 1
        if data[i][4] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Gęstość" + "]" + "®" + "(" + "Value" + ")" + "®" + "[" + str(data[i][4]) + "]\n"
            n = n + 1
        L.append(tmp)
    return L

def trainingDataToCGIF():
    L = list()
    for i in range(len(data)):
        tmp = "(" + "Attr" + " "
        if data[i][0] != 0:
            tmp = tmp + "[" + "Przetapialne" + "] "
        if data[i][1] != 0:
            tmp = tmp + "[" + "Tłuczenie" + "] "
        if data[i][2] != 0:
            tmp = tmp + "[" + "Przezroczyste" + "] "
        if data[i][3] != 0:
            tmp = tmp + "[" + "Wilgotne" + "] "
        if data[i][4] != 0:
            tmp = tmp + "[" + "Gęstość" + " " + "*x" + "] "
        tmp = tmp + "[" + data[i][5] + "]) "
        tmp = tmp + "(" + "Value" + " ?x " + "[" + str(data[i][4]) + "])\n"  
        L.append(tmp)
    return L

def trainingDataToKIF():
    L = list()
    for i in range(len(data)):
        tmpList = list()
        tmpList = [0]*5
        tmp = "(" + "exists" + " ("
        tmp = tmp + "(?x " + data[i][5] + ") "
        if data[i][0] != 0:
            tmp = tmp + "(?a " + "przetapialne" + ") "
            tmpList[0] = 1
        if data[i][1] != 0:
            tmp = tmp + "(?b " + "tłuczenie" + ") "
            tmpList[1] = 1
        if data[i][2] != 0:
            tmp = tmp + "(?c " + "przezroczyste" + ") "
            tmpList[2] = 1
        if data[i][3] != 0:
            tmp = tmp + "(?d " + "wilgotne" + ") "
            tmpList[3] = 1
        if data[i][4] != 0:
            tmp = tmp + "(?e " + "gęstość" + ") (?f " + str(data[i][4]) + ") "
        tmp = tmp + "(and (attr"
        if tmpList[0] == 1:
            tmp = tmp + " ?a"        
        if tmpList[1] == 1:
            tmp = tmp + " ?b"        
        if tmpList[2] == 1:
            tmp = tmp + " ?c"        
        if tmpList[3] == 1:
            tmp = tmp + " ?d"
        tmp = tmp + " ?e ?x) (value ?e ?f)))\n"
        L.append(tmp)
    return L

def trainingDataToCGFI2():
    L = list()
    for i in range(len(data)):
        tmpList = list()
        tmpList = [0]*5
        tmp = "[" + data[i][5] + " *x] "
        if data[i][0] != 0:
            tmp = tmp + "[Przetapialne *a" + "] "
            tmpList[0] = 1
        if data[i][1] != 0:
            tmp = tmp + "[Tłuczenie *b" + "] "
            tmpList[1] = 1
        if data[i][2] != 0:
            tmp = tmp + "[Przezroczyste *c" + "] "
            tmpList[2] = 1
        if data[i][3] != 0:
            tmp = tmp + "[Wilgotne *d" + "] "
            tmpList[3] = 1
        if data[i][4] != 0:
            tmp = tmp + "[Gęstość *e" + "] ["+ str(data[i][4]) + " *f]\n "
        tmp = tmp + "(attr"
        if tmpList[0] == 1:
            tmp = tmp + " ?a"        
        if tmpList[1] == 1:
            tmp = tmp + " ?b"        
        if tmpList[2] == 1:
            tmp = tmp + " ?c"        
        if tmpList[3] == 1:
            tmp = tmp + " ?d"
        tmp = tmp + " ?e ?x) (value ?e ?f)\n"
        L.append(tmp)
    return L

def createTranslatedFiles():
    a = open("dataLF.txt", "w+", encoding="utf-8")
    b = open("dataCGIF.txt", "w+", encoding="utf-8")
    c = open("dataCGFI2.txt", "w+", encoding="utf-8")
    d = open("dataKIF.txt", "w+", encoding="utf-8")

    datalen = len(trainingDataToLF())
    La = list(trainingDataToLF())
    Lb = list(trainingDataToCGIF())
    Lc = list(trainingDataToCGFI2())
    Ld = list(trainingDataToKIF())
    for i in range(datalen-1):
        a.write(La[i] + "\n")
        b.write(Lb[i] + "\n")
        c.write(Lc[i] + "\n")
        d.write(Ld[i] + "\n")

    a.close()
    b.close()
    c.close()
    d.close()

#trainingDataToLF()
#trainingDataToCGIF()
#trainingDataToKIF()
#trainingDataToCGFI2()

createTranslatedFiles()
