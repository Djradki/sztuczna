#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw, ImageFont
import os
import sys
import random
sys.path.insert(0, '../')

import translationCGIF

data = translationCGIF.getList()
datalen = len(data)
path = os.path.dirname(os.path.abspath(__file__))

def dataToLF():
    L = list()
    for i in range(datalen):
        n = 1
        tmp = "[" + data[i][7] + "]¬(Attr)" + "-\n"
        if data[i][0] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Przetapialne" + "]\n"
            n = n + 1
        if data[i][1] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Tłuczenie" + "]\n"
            n = n + 1
        if data[i][2] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Przezroczyste" + "]\n"
            n = n + 1
        if data[i][3] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Wilgotne" + "]\n"
            n = n + 1
        if data[i][4] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Niebezpieczne" + "]\n"
            n = n + 1
        if data[i][5] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Metaliczny" + "]\n"
            n = n + 1
        if data[i][6] != 0:
            tmp = tmp + "\t¬" + str(n) + "-[" + "Gęstość" + "]" + "®" + "(" + "Value" + ")" + "®" + "[" + str(data[i][6]) + "]\n"
            n = n + 1
        L.append(tmp)
    return L

def looptest():
    L = list()
    for i in range(datalen):
        tmp = data[i]
        L.append(tmp)
    return L

def dataToKIF():
    L = list()
    for i in range(datalen):
        tmpList = list()
        tmpList = [0]*7
        tmp = "(" + "exists" + " ("
        tmp = tmp + "(?x " + data[i][7] + ") "
        if data[i][0] != 0:
            tmp = tmp + "(?a " + "przetapialne" + ") "
            tmpList[0] = 1
        if data[i][1] != 0:
            tmp = tmp + "(?b " + "tłuczenie" + ") "
            tmpList[1] = 1
        if data[i][2] != 0:
            tmp = tmp + "(?c " + "przezroczyste" + ") "
            tmpList[2] = 1
        if data[i][3] != 0:
            tmp = tmp + "(?d " + "wilgotne" + ") "
            tmpList[3] = 1
        if data[i][4] != 0:
            tmp = tmp + "(?e " + "niebezpieczne" + ") "
            tmpList[4] = 1
        if data[i][5] != 0:
            tmp = tmp + "(?f " + "metaliczny" + ") "
            tmpList[5] = 1
        if data[i][6] != 0:
            tmp = tmp + "(?g " + "gęstość" + ") (?h " + str(data[i][6]) + ") "
        tmp = tmp + "(and (attr"
        if tmpList[0] == 1:
            tmp = tmp + " ?a"        
        if tmpList[1] == 1:
            tmp = tmp + " ?b"        
        if tmpList[2] == 1:
            tmp = tmp + " ?c"        
        if tmpList[3] == 1:
            tmp = tmp + " ?d"
        if tmpList[4] == 1:
            tmp = tmp + " ?e"
        if tmpList[5] == 1:
            tmp = tmp + " ?f"
        tmp = tmp + " ?g ?x) (value ?g ?h)))\n"
        L.append(tmp)
    return L

def dataToCGFI2():
    L = list()
    for i in range(datalen):
        tmpList = list()
        tmpList = [0]*6
        tmp = "[" + data[i][7] + " *x] "
        if data[i][0] != 0:
            tmp = tmp + "[Przetapialne *a" + "] "
            tmpList[0] = 1
        if data[i][1] != 0:
            tmp = tmp + "[Tłuczenie *b" + "] "
            tmpList[1] = 1
        if data[i][2] != 0:
            tmp = tmp + "[Przezroczyste *c" + "] "
            tmpList[2] = 1
        if data[i][3] != 0:
            tmp = tmp + "[Wilgotne *d" + "] "
            tmpList[3] = 1
        if data[i][4] != 0:
            tmp = tmp + "[Niebezpieczne *e" + "] "
            tmpList[4] = 1
        if data[i][5] != 0:
            tmp = tmp + "[Metaliczny *f" + "] "
            tmpList[5] = 1
        if data[i][6] != 0:
            tmp = tmp + "[Gęstość *g" + "] ["+ str(data[i][6]) + " *h]\n "
        tmp = tmp + "(attr"
        if tmpList[0] == 1:
            tmp = tmp + " ?a"        
        if tmpList[1] == 1:
            tmp = tmp + " ?b"        
        if tmpList[2] == 1:
            tmp = tmp + " ?c"        
        if tmpList[3] == 1:
            tmp = tmp + " ?d"
        if tmpList[4] == 1:
            tmp = tmp + " ?e"
        if tmpList[5] == 1:
            tmp = tmp + " ?f"
        tmp = tmp + " ?g ?x) (value ?g ?h)\n"
        L.append(tmp)
    return L

def dataToDisplayForm():
    for i in range(datalen):
        tmpList = list()
        tmpList = [0]*6
        itemname = data[i][7]
        filename = itemname.lower() + str(int(data[i][6]*10)) + "_" + str(i) + ".png"
        if data[i][0] != 0:
            tmpList[0] = 1
        if data[i][1] != 0:
            tmpList[1] = 1
        if data[i][2] != 0:
            tmpList[2] = 1
        if data[i][3] != 0:
            tmpList[3] = 1
        if data[i][4] != 0:
            tmpList[4] = 1
        if data[i][5] != 0:
            tmpList[5] = 1
        if tmpList.count(1) == 1:
            img = path+"/img_schemas/schema1.png"
        elif tmpList.count(1) == 2:
            img = path+"/img_schemas/schema2.png"
        elif tmpList.count(1) == 3:
            img = path+"/img_schemas/schema3.png"
        elif tmpList.count(1) == 4:
            img = path+"/img_schemas/schema4.png"
        image = Image.open(img)
        font_type = ImageFont.truetype("Arial.ttf", 18)
        draw = ImageDraw.Draw(image)
        tmpValList = list()
        if tmpList[0] == 1:
            tmpValList.append("Przetapialność")
        if tmpList[1] == 1:
            tmpValList.append("Tłuczenie")
        if tmpList[2] == 1:
            tmpValList.append("Przezroczystość")
        if tmpList[3] == 1:
            tmpValList.append("Wilgotność")
        if tmpList[4] == 1:
            tmpValList.append("Niebezpieczne")
        if tmpList[5] == 1:
            tmpValList.append("Metaliczny")

        if tmpList.count(1) == 1:
            draw.text(xy=(35,116),text=data[i][7],fill=(0,0,0),font=font_type)
            draw.text(xy=(177,116), text="Attr",fill=(0,0,0),font=font_type)
            draw.text(xy=(253,32),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
            draw.text(xy=(253,208),text="Gęstość",fill=(0,0,0),font=font_type)
            draw.text(xy=(420,208),text="Value",fill=(0,0,0),font=font_type)
            draw.text(xy=(551,208),text=str(data[i][6]),fill=(0,0,0),font=font_type)
            image.save(path+"/img_output/"+filename)

        if tmpList.count(1) == 2:
            draw.text(xy=(25,118),text=data[i][7],fill=(0,0,0),font=font_type)
            draw.text(xy=(183,118), text="Attr",fill=(0,0,0),font=font_type)
            draw.text(xy=(254,31),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
            draw.text(xy=(254,120),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
            draw.text(xy=(421,206),text="Value",fill=(0,0,0),font=font_type)
            draw.text(xy=(550,206),text=str(data[i][6]),fill=(0,0,0),font=font_type)
            draw.text(xy=(254,206),text="Gęstość",fill=(0,0,0),font=font_type)
            image.save(path+"/img_output/"+filename)

        if tmpList.count(1) == 3:
            draw.text(xy=(20,114),text=data[i][7],fill=(0,0,0),font=font_type)
            draw.text(xy=(170,114), text="Attr",fill=(0,0,0),font=font_type)
            draw.text(xy=(265,30),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
            draw.text(xy=(265,114),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
            draw.text(xy=(265,190),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
            draw.text(xy=(420,270),text="Value",fill=(0,0,0),font=font_type)
            draw.text(xy=(550,270),text=str(data[i][6]),fill=(0,0,0),font=font_type)
            draw.text(xy=(265,270),text="Gęstość",fill=(0,0,0),font=font_type)
            image.save(path+"/img_output/"+filename)

        if tmpList.count(1) == 4:
            draw.text(xy=(20,114),text=data[i][7],fill=(0,0,0),font=font_type)
            draw.text(xy=(170,114), text="Attr",fill=(0,0,0),font=font_type)
            draw.text(xy=(265,30),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
            draw.text(xy=(265,114),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
            draw.text(xy=(265,190),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
            draw.text(xy=(265,270),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
            draw.text(xy=(420,350),text="Value",fill=(0,0,0),font=font_type)
            draw.text(xy=(550,350),text=str(data[i][6]),fill=(0,0,0),font=font_type)
            draw.text(xy=(265,350),text="Gęstość",fill=(0,0,0),font=font_type)
            image.save(path+"/img_output/"+filename)

def createTranslatedFiles():
    a = open("dataLF.txt", "w+", encoding="utf-8")
    c = open("dataCGFI2.txt", "w+", encoding="utf-8")
    d = open("dataKIF.txt", "w+", encoding="utf-8")

    datalenx = len(dataToLF())
    La = list(dataToLF())
    Lc = list(dataToCGFI2())
    Ld = list(dataToKIF())
    for i in range(datalenx):
        a.write(La[i] + "\n")
        c.write(Lc[i] + "\n")
        d.write(Ld[i] + "\n")

    a.close()
    c.close()
    d.close()


#print(dataToLF())
#dataToKIF()
#dataToCGFI2()
createTranslatedFiles()
dataToDisplayForm()
#print(dataToLF())
