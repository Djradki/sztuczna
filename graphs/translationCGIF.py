import os
import re
import random
from PIL import Image, ImageDraw, ImageFont
path = os.path.dirname(os.path.abspath(__file__))
#przetapialność, tłuczenie, przezroczystość, wilgotność, gęstość
#tłumaczy dataCGIF.txt na listę

def getList():
    with open(path+"/dataCGIF.txt", "r", encoding="utf-8") as f:
        content = f.readlines()
    data = [x.strip() for x in content]
    while("" in data) : 
        data.remove("")
    f.close()
    for i in range(len(data)):
        tmp = list()
        if "Przetapialne" in data[i]:
            tmp.append(1)   
        else:
            tmp.append(0)   
        if "Tłuczenie" in data[i]:
            tmp.append(1)   
        else:
            tmp.append(0)  
        if "Przezroczyste" in data[i]:
            tmp.append(1)   
        else:
            tmp.append(0)  
        if "Wilgotne" in data[i]:
            tmp.append(1)   
        else:
            tmp.append(0)   
        if "Niebezpieczne" in data[i]:
            tmp.append(1)   
        else:
            tmp.append(0)  
        if "Metaliczny" in data[i]:
            tmp.append(1)   
        else:
            tmp.append(0)  
        tmp.append(float(''.join(map(str,re.findall(r"[-+]?\d*\.\d+|\d+", data[i])))))

        if "Papier" in data[i]:
            tmp.append("Papier")
        elif "Złom" in data[i]:
            tmp.append("Złom")
        elif "Plastik" in data[i]:
            tmp.append("Plastik")
        elif "Szkło" in data[i]:
            tmp.append("Szkło")
        elif "Bateria" in data[i]:
            tmp.append("Bateria")
        data[i] = tmp
    return data

def toCGIF(trashtype, trashinfo):
    trashinfo = str(trashinfo).replace("[", "").replace("]", "").strip().split(",")
    trashinfo = [float(i) for i in trashinfo]
    tmp = "(" + "Attr" + " "
    if trashinfo[0] != 0:
        tmp = tmp + "[" + "Przetapialne" + "] "
    if trashinfo[1] != 0:
        tmp = tmp + "[" + "Tłuczenie" + "] "
    if trashinfo[2] != 0:
        tmp = tmp + "[" + "Przezroczyste" + "] "
    if trashinfo[3] != 0:
        tmp = tmp + "[" + "Wilgotne" + "] "
    if trashinfo[4] != 0:
        tmp = tmp + "[" + "Niebezpieczne" + "] "
    if trashinfo[5] != 0:
        tmp = tmp + "[" + "Metaliczny" + "] "
    if trashinfo[6] != 0:
        tmp = tmp + "[" + "Gęstość" + " " + "*x" + "] "
    tmp = tmp + "[" + trashtype + "]) "
    tmp = tmp + "(" + "Value" + " ?x " + "[" + str(trashinfo[6]).lstrip() + "])\n"  
    return str(tmp)

def toDF(prediction, x, time):
    x = str(x).replace("[", "").replace("]", "").strip().split(",")
    x = [float(i) for i in x]
    tmpList = list()
    tmpList = [0]*6
    itemname = prediction
    filename = itemname.lower() + str(int(x[6]*100)) + "_" + str(random.randint(1,999999999)) + ".png"
    if x[0] != 0:
        tmpList[0] = 1
    if x[1] != 0:
        tmpList[1] = 1
    if x[2] != 0:
        tmpList[2] = 1
    if x[3] != 0:
        tmpList[3] = 1
    if x[4] != 0:
        tmpList[4] = 1
    if x[5] != 0:
        tmpList[5] = 1
    if tmpList.count(1) == 1:
        img = path+"/img_schemas/schema1.png"
    elif tmpList.count(1) == 2:
        img = path+"/img_schemas/schema2.png"
    elif tmpList.count(1) == 3:
        img = path+"/img_schemas/schema3.png"
    elif tmpList.count(1) == 4:
        img = path+"/img_schemas/schema4.png"
    image = Image.open(img)
    font_type = ImageFont.truetype(os.path.dirname(__file__)+"\\Arial.ttf", 18)
    draw = ImageDraw.Draw(image)

    draw.text(xy=(0,0),text=time,fill=(0,0,0),font=font_type)

    tmpValList = list()
    if tmpList[0] == 1:
        tmpValList.append("Przetapialność")
    if tmpList[1] == 1:
        tmpValList.append("Tłuczenie")
    if tmpList[2] == 1:
        tmpValList.append("Przezroczystość")
    if tmpList[3] == 1:
        tmpValList.append("Wilgotność")
    if tmpList[4] == 1:
        tmpValList.append("Niebezpieczne")
    if tmpList[5] == 1:
        tmpValList.append("Metaliczny")

    if tmpList.count(1) == 1:
        draw.text(xy=(35,116),text=prediction,fill=(0,0,0),font=font_type)
        draw.text(xy=(177,116), text="Attr",fill=(0,0,0),font=font_type)
        draw.text(xy=(253,32),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(253,208),text="Gęstość",fill=(0,0,0),font=font_type)
        draw.text(xy=(420,208),text="Value",fill=(0,0,0),font=font_type)
        draw.text(xy=(551,208),text=str(x[6]),fill=(0,0,0),font=font_type)
        image.save(path+"/../wysypisko/"+itemname.lower()+"/"+"/"+filename)

    if tmpList.count(1) == 2:
        draw.text(xy=(25,118),text=prediction,fill=(0,0,0),font=font_type)
        draw.text(xy=(183,118), text="Attr",fill=(0,0,0),font=font_type)
        draw.text(xy=(254,31),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(254,120),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(421,206),text="Value",fill=(0,0,0),font=font_type)
        draw.text(xy=(550,206),text=str(x[6]),fill=(0,0,0),font=font_type)
        draw.text(xy=(254,206),text="Gęstość",fill=(0,0,0),font=font_type)
        image.save(path+"/../wysypisko/"+itemname.lower()+"/"+filename)

    if tmpList.count(1) == 3:
        draw.text(xy=(20,114),text=prediction,fill=(0,0,0),font=font_type)
        draw.text(xy=(170,114), text="Attr",fill=(0,0,0),font=font_type)
        draw.text(xy=(265,30),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(265,114),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(265,190),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(420,270),text="Value",fill=(0,0,0),font=font_type)
        draw.text(xy=(550,270),text=str(x[6]),fill=(0,0,0),font=font_type)
        draw.text(xy=(265,270),text="Gęstość",fill=(0,0,0),font=font_type)
        image.save(path+"/../wysypisko/"+itemname.lower()+"/"+filename)

    if tmpList.count(1) == 4:
        draw.text(xy=(20,114),text=prediction,fill=(0,0,0),font=font_type)
        draw.text(xy=(170,114), text="Attr",fill=(0,0,0),font=font_type)
        draw.text(xy=(265,30),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(265,114),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(265,190),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(265,270),text=tmpValList.pop(0),fill=(0,0,0),font=font_type)
        draw.text(xy=(420,350),text="Value",fill=(0,0,0),font=font_type)
        draw.text(xy=(550,350),text=str(x[6]),fill=(0,0,0),font=font_type)
        draw.text(xy=(265,350),text="Gęstość",fill=(0,0,0),font=font_type)
        image.save(path+"/../wysypisko/"+itemname.lower()+"/"+filename)

#print(getList())
#print('\n'.join(['\t'.join([str(cell) for cell in row]) for row in getList()]))
