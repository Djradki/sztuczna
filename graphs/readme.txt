translationCGIF.py - zabiera funkcję która zwraca listę stworzoną z grafów z daneCGIF.txt
translateCGIFtoRest.py - zamienia CGIF na LF, DF, KIF i inną wersję CGIF
dataCGIF.txt - TO JEST GŁÓWNY PLIK, USUNIĘCIE SPOWODUJE BRAK DANYCH!

Changelog translateTDtoD.py:
    15 May 2019 23:00 - it translates trainingData.py to 4 conceptual graph forms: Linear Form, CGIF, KIF and CGIF (but based on CGIF and KIF)
    15 May 2019 23:38 - added saving graphs to output files
    16 May 2019 16:28 - added translation trainingData.py to Display Form (translateTDtoDisplayForm.py)
    16 May 2019 18:11 - added translation from CGIF to training data for our decision tree algorithm
    18 May 2019 21:11 - packed every translation to one file - old files moved to archives



To do list:
    done atm no ideas how to expand this folder
    
