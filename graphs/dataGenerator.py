#!/usr/bin/env python
# -*- coding: utf-8 -*-
#przetapialność, tłuczenie, przezroczystość, wilgotność, gęstość
#"(Attr [Wilgotne] [Gęstość *x] [Papier]) (Value ?x ["+str(i)+"])"
import os
import sys
import random
sys.path.insert(0, '../')

def genCGIFdata(trashtype, przet, tl, przez, wil, attr1, attr2, start, end):
    tmp = ''
    for i in range(start, end+1):
        tmp = tmp + "(Attr "
        if przet == 1:
            tmp = tmp + "[Przetapialne] "
        if tl == 1:
            tmp = tmp + "[Tłuczenie] "
        if przez == 1:
            tmp = tmp + "[Przezroczyste] "
        if wil == 1:
            tmp = tmp + "[Wilgotne] "
        if attr1 == 1:
            tmp = tmp + "[Niebezpieczne] "
        if attr2 == 1:
            tmp = tmp + "[Metaliczny] "
        tmp = tmp + "[Gęstość *x] "
        tmp = tmp + "[" + trashtype + "]) "
        tmp = tmp + "(Value ?x ["+str(i/10)+"])\n\n"
    print(tmp, end = '')

genCGIFdata("Papier", 0, 0, 0, 1, 0, 0, 9, 11)
genCGIFdata("Papier", 0, 0, 0, 1, 0, 0, 3, 5)
genCGIFdata("Plastik", 1, 0, 0, 0, 0, 0, 8, 23)
genCGIFdata("Plastik", 1, 0, 1, 0, 0, 0, 8, 23)
genCGIFdata("Szkło", 1, 1, 1, 0, 0, 0, 25, 59)
genCGIFdata("Złom", 1, 0, 0, 0, 0, 1, 75, 91)
genCGIFdata("Bateria", 0, 0, 0, 0, 1, 1, 21, 25)
genCGIFdata("Bateria", 0, 0, 0, 0, 1, 1, 51, 55)
genCGIFdata("Bateria", 0, 0, 0, 0, 1, 1, 71, 75)
