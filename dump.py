#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame as pg
import functions
import decisionTree as dt
from graphs import translationCGIF as graph
import datetime
import os
import threading
path = os.path.dirname(os.path.abspath(__file__))

class Dump(pg.sprite.Sprite):
    def __init__(self, x, y, window_width, window_height):
        pg.sprite.Sprite.__init__(self)

        self.window_width = window_width
        self.window_height = window_height

        # Set height, width
        self.image = pg.image.load(functions.getPath() + '/images/dump.png')

        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        #po co tyle list XD
        self.__papier = list()
        self.__plastik = list()
        self.__szklo = list()
        self.__zlom = list()
        #ilość każdego śmiecia w super nieskończonym wysypiskowym kontenerze danego typu
        self.__papiercount = 0
        self.__szklocount = 0
        self.__zlomcount = 0
        self.__plastikcount = 0
        self.__bateriacount = 0

        
        self.__tree = dt.buildTree(graph.getList())

        #zerowanie plików z logami
        open(path+"/logs/papier.txt", 'w').close()
        open(path+"/logs/plastik.txt", 'w').close()
        open(path+"/logs/szklo.txt", 'w').close()
        open(path+"/logs/zlom.txt", 'w').close()
        open(path+"/logs/bateria.txt", 'w').close()

        #zerowanie wysypiska
        for file in os.listdir(path+"/wysypisko/plastik"):
            if file.endswith('.png'):
                os.remove(path+"/wysypisko/plastik/"+file) 
        for file in os.listdir(path+"/wysypisko/złom"):
            if file.endswith('.png'):
                os.remove(path+"/wysypisko/złom/"+file) 
        for file in os.listdir(path+"/wysypisko/papier"):
            if file.endswith('.png'):
                os.remove(path+"/wysypisko/papier/"+file) 
        for file in os.listdir(path+"/wysypisko/szkło"):
            if file.endswith('.png'):
                os.remove(path+"/wysypisko/szkło/"+file) 
        for file in os.listdir(path+"/wysypisko/bateria"):
            if file.endswith('.png'):
                os.remove(path+"/wysypisko/bateria/"+file) 
    

    def take_trash(self, trashList):
        #import dumpWindow
        #dumpWindow.loadTrash(trashList)
        #print("trash taken")
        filePaper = open(path+"/logs/papier.txt", "a+", encoding="utf-8")
        filePlastic = open(path+"/logs/plastik.txt", "a+", encoding="utf-8")
        fileGlass = open(path+"/logs/szklo.txt", "a+", encoding="utf-8")
        fileJunk = open(path+"/logs/zlom.txt", "a+", encoding="utf-8")
        fileBattery = open(path+"/logs/bateria.txt", "a+", encoding="utf-8")
        
        while trashList:
            trash = trashList.pop(0)
            prediction = dt.classify(trash.get_trash(), self.__tree)
            key = list( prediction.keys() )[0]
            now = datetime.datetime.now()
            time_now = str(now.day)+"/"+str(now.month)+"/"+str(now.year)+" "+str(now.hour)+":"+str(now.minute)+":"+str(now.second)
            if key == 'Papier':
                #self.__papier.append(trash)
                self.__papiercount = self.__papiercount + 1
                filePaper.write(str(time_now)+"\ninfo: "+str(trash)+"\nCGIF before:"+str(graph.toCGIF("?",trash))+" prediction: " + key + "\nCGIF: "+str(graph.toCGIF(key,trash))+"\n\n")
            elif key == 'Plastik':
                #self.__plastik.append(trash)
                filePlastic.write(str(time_now)+"\ninfo: "+str(trash)+"\nCGIF before:"+str(graph.toCGIF("?",trash))+" prediction: " + key + "\nCGIF: "+str(graph.toCGIF(key,trash))+"\n\n")
                self.__plastikcount = self.__plastikcount + 1
            elif key == 'Szkło':
                #self.__szklo.append(trash)
                fileGlass.write(str(time_now)+"\ninfo: "+str(trash)+"\nCGIF before:"+str(graph.toCGIF("?",trash))+" prediction: " + key + "\nCGIF: "+str(graph.toCGIF(key,trash))+"\n\n")
                self.__szklocount = self.__szklocount + 1
            elif key == 'Złom':
                #self.__zlom.append(trash)
                fileJunk.write(str(time_now)+"\ninfo: "+str(trash)+"\nCGIF before:"+str(graph.toCGIF("?",trash))+" prediction: " + key + "\nCGIF: "+str(graph.toCGIF(key,trash))+"\n\n")
                self.__zlomcount = self.__zlomcount + 1
            elif key == 'Bateria':
                #self.__zlom.append(trash)
                fileBattery.write(str(time_now)+"\ninfo: "+str(trash)+"\nCGIF before:"+str(graph.toCGIF("?",trash))+" prediction: " + key + "\nCGIF: "+str(graph.toCGIF(key,trash))+"\n\n")
                self.__bateriacount = self.__bateriacount + 1
            #graph.toDF(key, trash, time_now) #zamiana na graf
            th = threading.Thread(target=graph.toDF, args=(key, trash, time_now))
            th.start()
            #print( dt.printLeaf(prediction) )
        filePaper.close()
        filePlastic.close()
        fileGlass.close()
        fileJunk.close()
        fileBattery.close()
        print("Wysypisko (ilość śmieci):\n\tPapier: {}\n\tPlastik: {}\n\tSzkło: {}\n\tZłom: {}\n\tBateria: {}".format(self.__papiercount, self.__plastikcount, self.__szklocount, self.__zlomcount, self.__bateriacount))
