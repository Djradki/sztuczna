from __future__ import print_function

#import trainingData
from graphs import translationCGIF as graph

#trainingData = trainingData.trainingSet
header = ["Przetapialność", "Tłuczenie", "Przezroczystość", "Wilgotność", "Niebezpieczne", "Metaliczny", "Gęstość"]

#zwraca unikalne wartości
def uniqueVals(rows, col):
    return set([row[col] for row in rows])

#liczy ile jest wyników i jakich
def classCount(rows):
    counts = {}
    for row in rows:
        label = row[-1]
        if label not in counts:
            counts[label] = 0
        counts[label] += 1
    return counts

#sprawdza czy wartość jest liczbowa
def isNumber(val):
    return isinstance(val, int) or isinstance(val, float)

class Question:
    def __init__(self, column, value):
        self.column = column
        self.value = value

    def match(self, example):
        val = example[self.column]
        if isNumber(val):
            return val >= self.value
        else:
            return val == self.value

    def __repr__(self):
        condition = "=="
        if isNumber(self.value):
            condition = ">="
        return "is %s %s %s?" %( header[self.column], condition, str(self.value) )

def partition(rows, question):
    trueRows, falseRows = [], []
    for row in rows:
        if question.match(row):
            trueRows.append(row)
        else:
            falseRows.append(row)

    return trueRows, falseRows

def calculateGini(rows):
    counts = classCount(rows)
    impurity = 1
    for lbl in counts:
        probability = counts[lbl] / float(len(rows))
        impurity -= probability**2
    return impurity

def infoGain(left, right, probability):
    p = float(len(left)) / (len(left) + len(right))
    return probability - p*calculateGini(left) - (1-p) * calculateGini(right)

def findBestSplit(rows):
    bestGain = 0
    bestQuestion = None
    currentUncertainty = calculateGini(rows)
    nOfColumns = len(rows[0]) - 1

    for column in range(nOfColumns):
        values = set([row[column] for row in rows])

        for value in values:
            question = Question(column, value)
            trueRows, falseRows = partition(rows, question)

            if len(trueRows) == 0 or len(falseRows) == 0:
                continue
            
            gain = infoGain(trueRows, falseRows, currentUncertainty)

            if gain >= bestGain:
                bestGain, bestQuestion = gain, question
    return bestGain, bestQuestion

#Leaf is at the botton and is a final thing
class Leaf:
    def __init__(self, rows):
        self.predictions = classCount(rows)

class DecisionNode:
    def __init__(self, question, trueBranch, falseBranch):
        self.question = question
        self.trueBranch = trueBranch
        self.falseBranch = falseBranch

def buildTree(rows):
    gain, question = findBestSplit(rows)

    if gain == 0:
        return Leaf(rows)
    
    trueRows, falseRows = partition(rows, question)
    trueBranch = buildTree(trueRows)
    falseBranch = buildTree(falseRows)

    return DecisionNode(question, trueBranch, falseBranch)

def printTree(node, spacing=""):
    if isinstance(node, Leaf):
        print(spacing + "Predict", node.predictions)
        return

    print(spacing + str(node.question))

    print(spacing + "--> True:")
    printTree(node.trueBranch, spacing + "  ")

    print(spacing + "--> False:")
    printTree(node.falseBranch, spacing + "  ")

def classify(row, node):
    if isinstance(node, Leaf):
        return node.predictions

    if node.question.match(row):
        return classify(row, node.trueBranch)
    else:
        return classify(row, node.falseBranch)

def printLeaf(counts):
    total = sum(counts.values()) * 1.0
    probabilities = {}
    for lbl in counts.keys():
        probabilities[lbl] = str(int(counts[lbl] / total * 100)) + "%"
    return probabilities

if __name__ == "__main__":
    #myTree = buildTree(trainingData)
    myTree = buildTree(graph.getList()) #pobiera dane z dataCGIF.txt zmienione na listę
    printTree(myTree)
    #print( printLeaf( classify([1, 0, 1, 1, 0.0], myTree) ) )

    #for row in trainingData:
        #print("Actual: %s. Predicted: %s" % ( row[-1], printLeaf(classify(row, myTree)) ) )
