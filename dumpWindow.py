import pygame as pg
import sys
import time
import functions as func
import robot

pg.init()

FPS = 30
fpsClock = pg.time.Clock()

_nof_tiles_height = 7
_nof_tiles_width = 4
WINDOW_WIDTH = _nof_tiles_width * 100
WINDOW_HEIGHT = _nof_tiles_height * 100
BACKGROUND = (100, 100, 100)  #RGB
_bottom_containers_y_pos = WINDOW_HEIGHT - 150
_robot_x_pos = 200
_robot_y_pos = 200

screen = pg.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 32)
pg.display.set_caption('Wysypisko')
screen.fill(BACKGROUND)

all_sprites_list = pg.sprite.Group()

### rozmiary obrazka 267*365
my_robot = robot.Robot(_robot_x_pos, _robot_y_pos)
all_sprites_list.add(my_robot)
truck = pg.image.load(func.getPath()+'/images/truck.png')
#robot = pg.image.load(func.getPath()+'\\images\\wysypisko_imgs\\robot.png')
container_papier = pg.image.load(func.getPath()+'/images/wysypisko_imgs/papier_container2.png')
container_plastik = pg.image.load(func.getPath()+'/images/wysypisko_imgs/plastik_container2.png')
container_szklo = pg.image.load(func.getPath()+'/images/wysypisko_imgs/szklo_container2.png')
container_zlom = pg.image.load(func.getPath()+'/images/wysypisko_imgs/zlom_container2.png')

#robot = pg.transform.scale(robot, (100, 100))
#container_papier = pg.transform.scale(container_papier, (100, 150))
#container_plastik = pg.transform.scale(container_plastik, (100, 150))
#container_szklo = pg.transform.scale(container_szklo, (100, 150))
#container_zlom = pg.transform.scale(container_zlom, (100, 150))

'''
screen.blit(truck, [WINDOW_WIDTH/2 - 50, 0])
#screen.blit(robot, [WINDOW_WIDTH/2-50, 250])
screen.blit(container_papier, [0, _bottom_containers_y_pos])
screen.blit(container_plastik, [100, _bottom_containers_y_pos])
screen.blit(container_szklo, [200, _bottom_containers_y_pos])
screen.blit(container_zlom, [300, _bottom_containers_y_pos])'''
###

move_right = True
trashes = list()

def loadTrash(trash_list):
    trashes = trash_list

while True:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            pg.quit()
            sys.exit()

    if move_right:
        if WINDOW_WIDTH > my_robot.rect.x+100:
            my_robot.move_right()
        else:
            move_right = False
            my_robot.move_left()
    else:
        if my_robot.rect.x <= 0:
            move_right = True
            my_robot.move_right()
        else:
            my_robot.move_left()

    all_sprites_list.update()
    screen.fill(BACKGROUND)

    if trashes:
        trashes.pop(0)
        print("I got trash")

    screen.blit(truck, [WINDOW_WIDTH / 2 - 50, -30])
    screen.blit(container_papier, [0, _bottom_containers_y_pos])
    screen.blit(container_plastik, [100, _bottom_containers_y_pos])
    screen.blit(container_szklo, [200, _bottom_containers_y_pos])
    screen.blit(container_zlom, [300, _bottom_containers_y_pos])
    time.sleep(0.1)

    all_sprites_list.draw(screen)
    pg.display.flip()
    fpsClock.tick(FPS)
