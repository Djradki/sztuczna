import pygame as pg
import functions


class Robot(pg.sprite.Sprite):

    def __init__(self, pos_x, pos_y):
        pg.sprite.Sprite.__init__(self)

        self.image = pg.image.load(functions.getPath() + '/images/wysypisko_imgs/robot.png')
        self.image = pg.transform.scale(self.image, (100, 100))
        self.rect = self.image.get_rect()
        self.rect.x = pos_x
        self.rect.y = pos_y

    def move_right(self):
        self.rect.x += 100

    def move_left(self):
        self.rect.x -= 100

