#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame as pg
import sys
import truck
import functions as fun
import a_star as ast
import time
# import random
# import operator

if __name__ == '__main__':

    roads, houses, walls, dump, city_map, size = fun.read_mapFile(fun.getPath()+'/maps/city3.txt')

    pg.init()

    FPS = 30
    fpsClock = pg.time.Clock()
    BACKGROUND = (0, 128, 0)
    WINDOW_WIDTH = size * 100
    WINDOW_HEIGHT = size * 100

    screen = pg.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 32)
    pg.display.set_caption('Śmieciarka')
    screen.fill(BACKGROUND)

    all_sprites_list = pg.sprite.Group()

    all_sprites_list.add(dump)

    for road in roads:
        all_sprites_list.add(road)
    for house in houses:
        house.generate_trash()
        all_sprites_list.add(house)
    for wall in walls:
        all_sprites_list.add(wall)

    truck = truck.Truck(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, walls, houses)

    graph = ast.AStarGraph(fun.get_walls(walls, houses, dump), 0, int(WINDOW_WIDTH/100) - 1)
    path = list()

    all_sprites_list.add(truck)

    while True:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()
            elif event.type == pg.KEYUP:
                house = fun.check_house_near(truck, houses)
                if event.key == pg.K_RIGHT:
                    truck.move_right()
                elif event.key == pg.K_LEFT:
                    truck.move_left()
                elif event.key == pg.K_DOWN:
                    truck.move_down()
                elif event.key == pg.K_UP:
                    truck.move_up()
                elif house and event.key == pg.K_SPACE:
                    #house.generate_trash() #kliknięcie spacji bierze śmieci, ale najpierw je generuje, trzeba będzie dodać sprawdzanie czy było się w danym domu
                    percentage = truck.take_trash(house) #bierze śmieci i zapisuje do zmiennej procentowe zapełnienie śmieciarki
                    if percentage >= 95.0: #jeśli śmieciarka jest zapełniona w 95% lub więcej to zmienia cel na wysypisko
                        start = (int(truck.rect.y/100), int(truck.rect.x/100))
                        end = (int(dump.rect.y/100), int(dump.rect.x/100))
                        path, cost = graph.AStarSearch(start, end)
                        print("go home")
                elif fun.is_near_dump(truck, dump) and event.key == pg.K_SPACE:
                    truck.throw_trash(dump)
        if path:
            move = path.pop(0)
            if move == 'right':
                truck.move_right()
            elif move == 'left':
                truck.move_left()
            elif move == 'down':
                truck.move_down()
            elif move == 'up':
                truck.move_up()
            if len(path) == 0:
                truck.throw_trash(dump)

        all_sprites_list.update()

        time.sleep(0.25)

        if pg.sprite.collide_rect(truck, dump):
            pg.quit()
            sys.exit()
        all_sprites_list.draw(screen)
        pg.display.flip()
        fpsClock.tick(FPS)


# test
'''if path:
    move = path.pop(0)
    if move == 'right':
        truck.move_right()
    elif move == 'left':
        truck.move_left()
    elif move == 'down':
        truck.move_down()
    elif move == 'up':
        truck.move_up()
else:
    start = (int(truck.rect.y/100), int(truck.rect.x/100))
    x = random.randint(0, len(houses)-1)
    end = (int(houses[x].rect.y/100), int(houses[x].rect.x/100))

    if tuple(map(operator.add, start, (1, 0))) != end and \
            tuple(map(operator.add, start, (-1, 0))) != end and \
            tuple(map(operator.add, start, (0, 1))) != end and \
            tuple(map(operator.add, start, (0, -1))) != end:
        path, cost = graph.AStarSearch(start, end)
        print(f'path: {path}\ncost: {cost}\n')
    else:
        path = list()'''
