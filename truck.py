#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame as pg
import functions


class Truck(pg.sprite.Sprite):
    def __init__(self, x, y, window_width, window_height, walls, houses):
        pg.sprite.Sprite.__init__(self)

        self.window_width = window_width
        self.window_height = window_height

        self.image = pg.image.load(functions.getPath() + "/images/truck.png")

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.step = 100

        self.walls = walls + houses
        self.houses = houses
        
        self.__total_capacity = 21000 #litry
        #self.__total_capacity = 500 #litry - test
        self.__container = list() #jest to lista śmieci w śmieciarce

    def move_right(self):
        self.image = pg.image.load(functions.getPath() + "/images/truck.png")
        if self.rect.x + self.rect.width + self.step <= self.window_width:
            self.rect.x += self.step
            for wall in self.walls:
                if self.rect.colliderect(wall.rect):
                    self.rect.x -= self.step
                    break

    def move_left(self):
        self.image = pg.image.load(functions.getPath() + "/images/truck2.png")
        if self.rect.x >= self.step:
            self.rect.x -= self.step
            for wall in self.walls:
                if self.rect.colliderect(wall.rect):
                    self.rect.x += self.step
                    break

    def move_down(self):
        self.image = pg.image.load(functions.getPath() + "/images/truck3.png")
        if self.rect.y + self.rect.height + self.step <= self.window_height:
            self.rect.y += self.step
            for wall in self.walls:
                if self.rect.colliderect(wall.rect):
                    self.rect.y -= self.step
                    break

    def move_up(self):
        self.image = pg.image.load(functions.getPath() + "/images/truck4.png")
        if self.rect.y >= self.step:
            self.rect.y -= self.step
            for wall in self.walls:
                if self.rect.colliderect(wall.rect):
                    self.rect.y += self.step
                    break

    #@staticmethod
    def take_trash(self, house):
        pickedTrash = house.pickTrash() #bierze śmieci z domu
        for item in pickedTrash:
            self.__container.append(item) #dodaje śmieci z kosza/domu do listy śmieci śmieciarki
        current_volume = 0.0 #początkowa objętość śmieci w śmieciarce
        for item in self.__container:
            current_volume += item.get_volume() #zapełnianie śmieciarki (objętości)
        percentage = int(current_volume/self.__total_capacity*10000) / 100 #procentowe zapełnienie śmieciarki current/21000
        print('Wziąłem smieci: {} litrów, zapełnienie: {}%'.format(current_volume, percentage))
        #return percentage #zabranie śmieci zwraca procentowe zapełnienie śmieciarki

    def throw_trash(self, dump):
        dump.take_trash(self.__container)

    def reset(self):
        self.rect.x = 0
        self.rect.y = 0

        
