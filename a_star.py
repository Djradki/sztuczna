class AStarGraph(object):

    def __init__(self, barriers, begin, end):
        self.barriers = barriers
        self.begin = begin
        self.end = end

    def move_cost(self, b, end):
        if b == end:
            return 1
        if b in self.barriers:
            return 100  # Extremely high cost to enter barrier squares
        return 1  # Normal movement cost

    @staticmethod
    def heuristic(start, goal):
        # Use Chebyshev distance heuristic if we can move 
        dx = abs(start[0] - goal[0])
        dy = abs(start[1] - goal[1])
        return max(dx, dy)

    def get_vertex_neighbours(self, pos):
        n = []

        for dx, dy in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
            x2 = pos[0] + dx
            y2 = pos[1] + dy
            if x2 < self.begin or x2 > self.end or y2 < self.begin or y2 > self.end:
                continue
            n.append((x2, y2))
        return n

    @staticmethod
    def dir_to_moves(path):
        moves = []
        for i in range(len(path)-1):
            check = (path[i+1][0] - path[i][0], path[i+1][1] - path[i][1])
            if check == (1, 0):
                moves.append('down')
            elif check == (-1, 0):
                moves.append('up')
            elif check == (0, 1):
                moves.append('right')
            elif check == (0, -1):
                moves.append('left')
        del moves[-1]
        return moves

    def AStarSearch(self, start, end):
        actuall_move_cost = dict()  # Actuall movement cost to each position from the start position
        estimated_move_cost = dict()  # Estimated movement cost of start to end going via this position
    
        # Initialize starting values
        actuall_move_cost[start] = 0
        estimated_move_cost[start] = self.heuristic(start, end)
    
        closedVertices = set()
        openVertices = {start}
        cameFrom = dict()
    
        while len(openVertices) > 0:
            # Get the vertex in the open list with the lowest estimated movement cost score
            current = None
            currentFscore = None
            for pos in openVertices:
                if current is None or estimated_move_cost[pos] < currentFscore:
                    currentFscore = estimated_move_cost[pos]
                    current = pos
    
            # Check if we have reached the goal
            if current == end:
                # Retrace our route backward
                path = [current]
                while current in cameFrom:
                    current = cameFrom[current]
                    path.append(current)
                path.reverse()
                return self.dir_to_moves(path), estimated_move_cost[end], path[-2]  # path founded
    
            # Mark the current vertex as closed
            openVertices.remove(current)
            closedVertices.add(current)
    
            # Update scores for vertices near the current position
            for neighbour in self.get_vertex_neighbours(current):
                if neighbour in closedVertices:
                    continue  # We have already processed this node exhaustively
                candidateG = actuall_move_cost[current] + self.move_cost(neighbour, end)
    
                if neighbour not in openVertices:
                    openVertices.add(neighbour)  # Discovered a new vertex
                elif candidateG >= actuall_move_cost[neighbour]:
                    continue  # This actuall movement cost score is worse than previously found
    
                # Adopt this actuall movement cost score
                cameFrom[neighbour] = current
                actuall_move_cost[neighbour] = candidateG
                heuristic_value = self.heuristic(neighbour, end)
                estimated_move_cost[neighbour] = actuall_move_cost[neighbour] + heuristic_value

        raise RuntimeError("A Star algorithm failed to find a solution")
