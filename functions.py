import road
import house
import grass
import dump as dump_
import os


def getPath():
    return os.path.dirname(__file__)


def check_house_near(truck, houses):
    #print(f'{(truck.rect.x, truck.rect.y)}')
    for _house in houses:
        if [truck.rect.x + 100, truck.rect.y] == [_house.rect.x, _house.rect.y]\
                or [truck.rect.x - 100, truck.rect.y] == [_house.rect.x, _house.rect.y]\
                or [truck.rect.x, truck.rect.y + 100] == [_house.rect.x, _house.rect.y]\
                or [truck.rect.x, truck.rect.y - 100] == [_house.rect.x, _house.rect.y]:
            return _house
    return 'omg'


def is_near_dump(truck, dump):
    if [truck.rect.x + 100, truck.rect.y] == [dump.rect.x, dump.rect.y]\
            or [truck.rect.x - 100, truck.rect.y] == [dump.rect.x, dump.rect.y]\
            or [truck.rect.x, truck.rect.y + 100] == [dump.rect.x, dump.rect.y]\
            or [truck.rect.x, truck.rect.y - 100] == [dump.rect.x, dump.rect.y]:
        return True
    return False


def read_mapFile(path):
    roads, houses, walls = list(), list(), list()
    city_map = list()
    dump = None

    with open(path) as file:
        for line in file:
            city_map.append(list(line.split()))

    size = len(city_map)

    for i in range(len(city_map)):
        for j in range(len(city_map)):
            if city_map[i][j] == 'R':
                roads.append(road.Road(j * 100, i * 100))
            elif city_map[i][j] == 'H':
                houses.append(house.House(j * 100, i * 100))
            elif city_map[i][j] == 'G':
                walls.append(grass.Grass(j * 100, i * 100))
            elif city_map[i][j] == 'D':
                dump = dump_.Dump(j * 100, i * 100, size*100, size*100)

    return roads, houses, walls, dump, city_map, size


def get_walls(walls, houses, dump):
    barriers = list()
    for wall in walls:
        barriers.append((int(wall.rect.y/100), int(wall.rect.x/100)))
    for _house in houses:
        barriers.append((int(_house.rect.y/100), int(_house.rect.x/100)))
    barriers.append((int(dump.rect.y/100), int(dump.rect.x/100)))
    return barriers


def find_paths(graph, truck, houses, dump):
    paths = list()
    h_coords = [(int(h.rect.y / 100), int(h.rect.x / 100)) for h in houses]
    d_coords = (int(dump.rect.y / 100), int(dump.rect.x / 100))
    start = (int(truck.rect.y / 100), int(truck.rect.x / 100))
    while h_coords:
        to_check = list()
        for coord in h_coords:
            path, cost, pos = graph.AStarSearch(start, coord)
            path.append('take')
            to_check.append([cost, path, pos, coord])
        to_check.sort(key=lambda x: x[0])
        start = to_check[0][2]
        paths.append(to_check[0][1])
        h_coords.remove(to_check[0][3])

    path, cost, pos = graph.AStarSearch(start, d_coords)
    path.append('throw')
    paths.append(path)

    return paths
